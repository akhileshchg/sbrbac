#include <linux/fs.h>
#include <asm/segment.h>
#include <linux/uaccess.h>
#include <linux/buffer_head.h>
#include <linux/linkage.h>
#include <linux/moduleloader.h>
#include <linux/slab.h>
/*
 *openfile -    Opens file in kernel space.
 *		on failure, returns appropriate error and sets @out arg to NULL
 *@out		: The file pointer address which references the opened file
 *@filename     : The name of the file to be opened.
 *@flags	: The flags used to open file. Refer to flags in open(2) call.
 *@mode		: The mode used to open file. Refer to mode in open(2) call.
 */
int openfile(struct file **out, const char *filename, int flags, int mode)
{
	int ret = 0;
	*out = filp_open(filename, flags, mode);
	if (!(*out) || IS_ERR(*out)) {
		ret = PTR_ERR(*out);
		*out = NULL;
	}
	return ret;
}
/*
 *Closefile -   Closes file in kernel space.
 *@out		: File pointer address which references the file to be closed.
 */
void closefile(struct file **out)
{
	if (*out)
		filp_close(*out, NULL);
	*out = NULL;
}
/*
 *readfile -    Reads @size bytes from open file into buffer
 *		Returns appropriate error on failure.
 *@fd		: The open file reference from which data is read.
 *@buf		: The buffer into which the data is copied
 *@size		: The amount of bytes that must be read
 */
int readfile(struct file **fd, char *buf, int size)
{
	int bytes  = -EBADF;
	if (*fd) {
		mm_segment_t oldfs;
		oldfs = get_fs();
		set_fs(KERNEL_DS);
		bytes = vfs_read(*fd, buf, size, &(*fd)->f_pos);
		set_fs(oldfs);
	}
	return bytes;
}
/*
 *writefile -	Writes @size bytes to open file from buffer
 *		Returns appropriate error on failure.
 *@fd		: The open file reference to which data is written.
 *@buf		: The buffer from which the data is written
 *@size		: The amount of bytes that must be written
 */
int writefile(struct file **fd, char *buf, int size)
{
	int bytes = -EBADF;
	if (*fd) {
		mm_segment_t oldfs;
		oldfs = get_fs();
		set_fs(KERNEL_DS);
		bytes = vfs_write(*fd, buf, size, &(*fd)->f_pos);
		set_fs(oldfs);
	}
	return bytes;
}
