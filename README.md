Design and Implementation Document of  SBRBAC
============================================================================
Implementation Environment
--------------------------
Linux Vanilla Kernel 3.14.17 64 bit.

Design & Implementation:
------------------------
Like any other rbac system, SBRBAC uses roles for transactions(permissions for
access on objects). In this design, we can create(delete) roles, add users(delete)
to(from) a role, add(remove) transaction (<filepath>:<set of ops>) to(from) a 
role, add user to multiple roles and then (un)set active role for a user. 
The list of operations that are mediated in inode access include create, unlink
, hardlink, symlink, mkdir, rmdir,rename. There is default role to which, by
default, all the users are members. The default roles lists set of directories 
where, all users have all permissions to do any activity. This is typical
setting of scratch space for test runs in industry. Ofcourse, the existing DAC
security would still be in place.Also, any file newly created based on the 
transactions allowed on its parent will inherit the parents security policies.
Also, all the mediation operations are done on inode using a standard security 
framework provided by linux called the Linux Security Modules. This module gives
exhaustive set of hooks at different layers to mediate the access. This project
merely overwrites the functions of inode mediation. The list of config files
are stored /etc/sbrbac/ directory.


Any system supporting RBAC must be specifying policies for every object in its
environment. The scope of SBRBAC may not support this, i.e. the system
may break if proper care is not taken on specifying rules for objects essential
for booting up the system. Hence an option called enforce is provided with user
utlity. This enforce option takes an existing directories as input. Once the
enforce is set, the rbac system acts only those files whose ancestor is the one
of the directory set during enforce. One can also change the enforce directory,
But remember one may have to significantly change the role permissions. Because 
of the time constraint, the role inheritance has not been developed

Usage:
------
To add role
./sbrbac.py addrole <role1...>

To delete role:
./sbrbac.py delrole <role1...>

To add user to a role:
./sbrbac.py adduser -u <user1,user2...> -r <role1,...>

To delete user from a role:
./sbrbac.py deluser -u <user1,user2...> -r <role1,...>

To add transaction to a role:
./sbrbac.py addtransaction -t <full filepath:<list among legally supported functions> -r list

To delete transaction from a role:
./sbrbac.py deltransaction -t <full filepath:<list among legally supported functions> -r list

To add active role to user:
./sbrbac.py setactive -r <user1,...> -u role

To delete activerole from user:
././sbrbac.py unsetactive -r <user1,...> -u role

To show the list of all users:
./sbrbac.py shorole

To show the list of users for a role:
./sbrbac.py showusers <role1,...>

To show the list of transactions for role:
./sbrbac.py showtrans <role1,...>

To show the active roles of users:
./sbrbac.py showactiveroles <user1,...>

To enforce the list directories for sbrbac:
./sbrbac.py enforce <dir1,...>
