#!/usr/bin/env python
import sys
import os
import pwd
'''
This user utility is developed as part of Stony Brook RBAC security system.
Users can add roles, users to created roles, transactions to created roles.
The tool also supports modify, delete in place of create in above functions.

Plans to implement
------------------
The utility can also be used to establish inheritance relations among roles
'''
def rbac_help():
	print "Usage:\n For creating roles:\n./sbrbac addrole <role1, role2, ...>\n"
	print "For adding users to role:\n./sbrbac addrole -u user1,... -r role1,...\n"
	print "For adding transactions to role:\n./sbrbac addtransaction -t <fullpath>:[create,unlink,mkdir,rmdir,rename,link,symlink]\n"
	print "For deleting role:\n./sbrbac delrole <role1,...>\n"
	print "For deleting users from role:\n./sbrbac delrole -u user1,... -r role1,...\n"
	print "For deleting transactions to role:\n./sbrbac addtransaction -t <fullpath>:[create,unlink,mkdir,rmdir,rename,link,symlink]\n"
	print "For printing all roles:\n./sbrbac showrole\n"
	print "For printing all users of roles:\n./sbrbac showusers <role1,...>\n"
	print "For printing all transactions of roles:\n./sbrbac showtrans <role1,...>\n"
	print "For setting active role: ./sbrbac setactive -u <userlist> -r <one role>\n"
	print "For unsetting active role: ./sbrbac unsetactive -u <userlist>\n"

def getroles():
	return os.listdir("/etc/sbrbac/roles")
	
def addroles(roles):
	for role in roles:
		if role == 'default':
			continue;
		if not os.path.isfile("/etc/sbrbac/roles/"+role):
			f = open("/etc/sbrbac/roles/"+role, 'w')
			f.close()
			print "New role "+role+" created"
		else:
			print role+" already exists"
def getusers(role):
	users = []
	if role == 'default':
		print 'All the users are by default in "default" role'
	elif not os.path.isfile("/etc/sbrbac/roles/"+role):
		print "Looks like the role "+role+" does not exist."
	else:
		f = open("/etc/sbrbac/roles/"+role, 'r')
		line = f.readline()
		f.close()
		if len(line) != 0:
			uids = line.strip().split(',')
			users = [pwd.getpwuid(int(uid)).pw_name for uid in uids]
	return users	
		
def delroles(roles):
	for role in roles:
		if role == 'default':
			continue
		elif not os.path.isfile("/etc/sbrbac/roles/"+role):
			print "Looks like the role "+role+" does not exist. Trying remaining roles"
		else:
			users = getusers(role);
			for user in users:
				if getactive(user) == role:
					unsetactive([user])
			os.remove("/etc/sbrbac/roles/"+role)
			if os.path.isfile("/etc/sbrbac/trans/"+role):
				os.remove("/etc/sbrbac/trans/"+role)
		#delete roles file and trans file
def modifyusers(roles, users, check):
	for role in roles:
		if role == 'default':
			continue
		if not os.path.isfile("/etc/sbrbac/roles/"+role):
			print "Looks like the role "+role+" does not exist. Trying remaining roles"
		else:
			f = open("/etc/sbrbac/roles/"+role, 'r+')
			lines = [l.strip() for l in f]
			if len(lines) == 0:
				f.write(','.join([str(pwd.getpwnam(user).pw_uid) for user in users]))
			else:
				new_users = {}
				if check is True:
					new_users = set(lines[0].split(',')).union({str(pwd.getpwnam(user).pw_uid) for user in users})
				else:
					new_users = set(lines[0].split(',')).difference({str(pwd.getpwnam(user).pw_uid) for user in users})
					del_users = set(lines[0].split(',')).intersection({str(pwd.getpwnam(user).pw_uid) for user in users})
					unsetactive([pwd.getpwuid(int(iden)).pw_name for iden in list(del_users)])
				f.seek(0)
				f.truncate()
				f.write(','.join(list(new_users)))
			f.close()
			
def check_transaction(role, trans):
	sol = []
	ops = {'create', 'unlink', 'mkdir', 'rmdir', 'rename', 'hardlink', 'symlink'}
	if role == 'default':
		for tran in trans:
			if os.path.exists(tran):
				sol.append(tran)
		return sol
	else:
		for tran in trans:
			pair = tran.split(':')
			if os.path.exists(pair[0]):
				tran_ops = set(pair[1].split(','))
				if tran_ops.issubset(ops):
					sol.append(tran)
				else:
					print "Transaction "+tran+" is invalid\n"
		return sol
def update_transactions(role, old_trans, new_trans, check):
	updated_trans = []
	if role != 'default':
		dtrans = dict()
		for tran in old_trans:
			pair = tran.split(':')
			dtrans[pair[0]] = set(pair[1].split(','))
		for tran in new_trans:
			pair = tran.split(':')
			if pair[0] in dtrans:
				if check is True:
					dtrans[pair[0]] = dtrans[pair[0]].union(set(pair[1].split(',')))
				else:
					dtrans[pair[0]] = dtrans[pair[0]].difference(set(pair[1].split(',')))
			else:
				dtrans[pair[0]] = set(pair[1].split(','))	
		for key,value in dtrans.iteritems():
			if len(value) != 0:
				updated_trans.append(':'.join([key,','.join(list(value))]))
		return updated_trans
	else:
		if check is True:
			return list(set(old_trans).union(set(new_trans)))
		else:
			return list(set(old_trans).difference(set(new_trans)))		
	
def modifytransactions(roles, trans, check):
	for role in roles:
		if role!='default' and not os.path.isfile("/etc/sbrbac/roles/"+role):
			print "Looks like the role "+role+" does not exist. Trying remaining roles"
		else:
			try:
				f = open("/etc/sbrbac/trans/"+role, 'r+')
			except IOError:
				f = open("/etc/sbrbac/trans/"+role, 'w+')
			lines = [line.strip() for line in f]
			if len(lines) == 0:
				changed_trans = check_transaction(role, trans)
				f.write('\n'.join(changed_trans))
			else:
				old_trans = lines[:]
				changed_trans = check_transaction(role, trans)
				new_trans = update_transactions(role, old_trans, changed_trans, check)
				f.seek(0)
				f.truncate()
				f.write('\n'.join(new_trans))
			f.close()
def gettrans(role):
	trans=[]
	if role != 'default' and not os.path.isfile("/etc/sbrbac/roles/"+role):
		print "Looks like the role "+role+" does not exist. Trying remaining roles"
	else:
		try:
			f = open("/etc/sbrbac/trans/"+role, 'r')
		except IOError:
			return trans
		lines = f.readlines()
		if len(lines) != 0:
			trans = lines[:]
	return trans
def getactive(user):
	role = 'default'
	try:
		f = open("/etc/sbrbac/active/active", 'r+')
	except IOError:
		f = open("/etc/sbrbac/active/active", 'w+')
	uid = str(pwd.getpwnam(user).pw_uid)
	lines = [l.strip() for l in f]
	old_users = [l.split(':')[0] for l in lines]
	if uid in old_users:
		role = lines[old_users.index(uid)].split(':')[1]
	f.close()
	return role
def setenforce(dirs, check):
	try:
                f = open("/etc/sbrbac/active/enforce", 'r+')
        except IOError:
                f = open("/etc/sbrbac/active/enforce", 'w+')
	lines = [l.strip() for l in f];
	if (len(lines) == 0):
		f.write('\n'.join(dirs))
	else:
		new_trans = update_transactions('default',lines,dirs, check)
		f.seek(0)
		f.truncate()
		if len(new_trans) != 0:
			f.write('\n'.join(new_trans))
	f.close()
def getenforce():
	try:
		f = open("/etc/sbrbac/active/enforce", 'r')
	except IOError:
		return  "No enforcements defined"
	lines = [l.strip() for l in f]
	f.close()
	return '\n'.join(lines)
	
def unsetactive(users):
	try:
		f = open("/etc/sbrbac/active/active", 'r+')
	except IOError:
		f = open("/etc/sbrbac/active/active", 'w+')
	ids = [str(pwd.getpwnam(user).pw_uid) for user in users]
	lines = [l.strip() for l in f]
	old_users = [l.split(':')[0] for l in lines]
	for user in ids:
		if user in old_users:
			lines.pop(old_users.index(user))
	f.seek(0)
	f.truncate()
	f.write('\n'.join(lines))
	f.close()
def setactive(role, users):
	if role == 'default':
		unsetactive(users)
	else:
		#check if the user is part of role in /etc/sbrbac/roles/role
		f = open("/etc/sbrbac/roles/"+role, 'r')
		line = f.readline()
		f.close()
		ids = [str(pwd.getpwnam(user).pw_uid) for user in users]
		for user in ids:
			if not user in line:
				print user+" is not part of "+role
			else:
				try:
					f = open("/etc/sbrbac/active/active", 'r+')
				except IOError:
					f = open("/etc/sbrbac/active/active", 'w+')
				lines = [l.strip() for l in f]
				old_users = [l.split(':')[0] for l in lines]
				if user in old_users:
					lines[old_users.index(user)] = user+":"+role
				else:
					lines.append(user+':'+role)
				f.seek(0)
				f.truncate()
				f.write('\n'.join(lines))
				f.close()
if __name__ == '__main__':
	#list of command
	if len(sys.argv) < 2:
		rbac_help()
		exit()
	elif sys.argv[1] == 'addrole':
		roles = sys.argv[2:]
		addroles(roles);		
	elif sys.argv[1] == 'adduser':
		if sys.argv[2] == '-u':
			users = sys.argv[3:sys.argv.index('-r')]
			roles = sys.argv[sys.argv.index('-r')+1:]
			modifyusers(roles, users, True)
	elif sys.argv[1] == 'addtransaction':
		if sys.argv[2] == '-t':
			trans = sys.argv[3:sys.argv.index('-r')]
			roles = sys.argv[sys.argv.index('-r')+1:]
			modifytransactions(roles, trans, True)
	elif sys.argv[1] == 'deluser':
		if sys.argv[2] == '-u':
			users = sys.argv[3:sys.argv.index('-r')]
			roles = sys.argv[sys.argv.index('-r')+1:]
			modifyusers(roles, users, False)
	elif sys.argv[1] == 'deltransaction':
		if sys.argv[2] == '-t':
			trans = sys.argv[3:sys.argv.index('-r')]
			roles = sys.argv[sys.argv.index('-r')+1:]
			modifytransactions(roles, trans, False)
	elif sys.argv[1] == 'setactive':
		if sys.argv[2] == '-u':
			users = sys.argv[3:sys.argv.index('-r')]
			roles = sys.argv[sys.argv.index('-r')+1]
			setactive(roles, users)
	elif sys.argv[1] == 'unsetactive':
		if sys.argv[2] == '-u':
			users = sys.argv[3:]
			unsetactive(users)
	elif sys.argv[1] == 'delrole':
		roles = sys.argv[2:]
		delroles(roles)
	elif sys.argv[1] == 'showrole':
		print "Roles in SBRBAC:\n"
		print ' '.join(getroles())
	elif sys.argv[1] == 'showusers':
		roles = sys.argv[2:]
		for role in roles:
			if role == 'default':
				print "Users for Role: default\nAll users of the system"
			elif not role in os.listdir('/etc/sbrbac/roles'):
				print "Role "+role+" does not exist"
			else:
				print "Users for Role: "+role+'\n'
				print '\n'.join(getusers(role))
			print '---------------------------------------------------\n'
	elif sys.argv[1] == 'showactiveroles':
		users = sys.argv[2:]
		for user in users:
			print "Active user for "+user+" = "+getactive(user)
	elif sys.argv[1] == 'showtrans':
		roles = sys.argv[2:]
		for role in roles:
			if role !='default' and not role in os.listdir('/etc/sbrbac/roles'):
				print "Role "+role+" does not exist"
			else:
				print "Transactions for Role "+role+'\n'
				print '\n'.join(gettrans(role))
			print '---------------------------------------------------\n'
	elif sys.argv[1] == 'enforce':
		dirs = sys.argv[2:]
		setenforce(dirs, True)
	elif sys.argv[1] == 'rmenforce':
		dirs = sys.argv[2:]
                setenforce(dirs, False)
	elif sys.argv[1] == 'showenforce':
		print getenforce()
	elif sys.argv[1] == 'init':
		if not os.path.exists('/etc/sbrbac'):
			os.makedirs('/etc/sbrbac')
		if not os.path.exists('/etc/sbrbac/roles'):
			os.makedirs('/etc/sbrbac/roles')
		if not os.path.exists('/etc/sbrbac/active'):
			os.makedirs('/etc/sbrbac/active')
		if not os.path.exists('/etc/sbrbac/active/active'):
			f = open('/etc/sbrbac/active/active', 'w+');
			f.close()
		if not os.path.exists('/etc/sbrbac/active/enforce'):
			f = open('/etc/sbrbac/active/enforce', 'w+')
			f.close()
		if not os.path.exists('/etc/sbrbac/trans'):
			os.makedirs('/etc/sbrbac/trans')
		if not os.path.exists('/etc/sbrbac/trans/default'):
			f = open('/etc/sbrbac/trans/default', 'w+')
			f.close()
	else:
		rbac_help()
