/*
 * 
 * SBRBAC: A Minimal RBAC system implemented as part of CSE 509 at Stony Brook
 *
 * Author: Akhilesh Chaganti <achaganti@cs.stonybrook.edu>
 *
 */
#include <linux/fs.h>
#include <linux/security.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/cred.h>
#include <linux/string.h>

#include "file_ops.h"
#define AUTHOR "Akhilesh Chaganti"

#define CONFIG_PARENT "/etc/"
#define CONFIG_DIR CONFIG_PARENT"/sbrbac/"
#define CONFIG_ACTIVE CONFIG_DIR"/active/"
#define CONFIG_ROLE CONFIG_DIR"/roles/"
#define CONFIG_TRAN CONFIG_DIR"/trans/"
#define CONFIG_TRAN_DEFAULT CONFIG_TRAN"/default"
#define CONFIG_ENFORCE CONFIG_ACTIVE"/enforce"
bool role_exists(char* role)
{
	return true;
}

void get_active_role(uid_t id, char** role)
{
	struct file *out = NULL;
	char *buf = NULL;
	char uid_str[256];
	int size = 4098;
	int ret = 0;
	int i = 0;
	char *result = NULL;
	if (id == 0) {
		strcpy(*role, "root");
	}else {
		/*
		 * Open CONFIG_ACTIVE/active file. Check for the id:role
		 * If not found return default. Else,
		 * check if the corresponding role exists, if yes return,
		 * else return default
		 */
		ret = openfile(&out, CONFIG_ACTIVE"/active", O_RDONLY, 0);
		if (ret) {
			printk("SBRBAC: No active file present\n");
			goto failsafe;
		}else{
			/*
			 * For now get the size of file and read the entire file
			 * into buffer
			 */
			if (out->f_dentry->d_inode->i_size > size)
				size = out->f_dentry->d_inode->i_size + 1;
			buf = (char*) kmalloc(size, GFP_KERNEL);
			ret = readfile(&out, buf, size);
			if (ret <= 0) {
				goto failsafe;
			}else{
				/*search buf with "id:"*/
				buf[ret] = '\0';
				printk("buf = %s\n", buf);
				sprintf(uid_str, "%d:", (unsigned int)id);
				result = strstr(buf, uid_str);
				if (!result) {
					goto failsafe;
				}else{
					for (i = 0; i < strlen(result); i++){
						if (result[i] == '\n'){
							result[i] = '\0';
							break;
						}
					}
					result = &result[strlen(uid_str)];
					printk("result = %s\n", result);
					if(role_exists(result)) {
						strcpy(*role, result);
						kfree(buf);
						closefile(&out);
						return;
					} else {
						goto failsafe;
					}
				}
			}		
		}
failsafe:
		if (buf)
			kfree(buf);
		if (out)
			closefile(&out);
		strcpy(*role, "default");
	}
}
void __getpath(struct dentry *dentry, char** path, int *size)
{
	if(strcmp(dentry->d_name.name, "/")) {
		*size = *size + strlen(dentry->d_name.name) + 1;
		__getpath(dentry->d_parent, path, size);
		strcat(*path, "/");
		strcat(*path, dentry->d_name.name);
	} else {
		*path = (char *)kmalloc(*size, GFP_KERNEL);
		strcpy(*path, "\0");
	}
}
void getpath(struct dentry *dentry, char **path)
{
	/*
	 * Yet to implement, presently copies "/home/default"
	 */
	 int size = 1;
	 __getpath(dentry, path, &size);
}
int check_path_with_default(char *path)
{
	/*
	 * Open defaults file.
	 * If not able to, simply return the error.
	 * else, try reading full file, if not able to, return error.
	 * else, check if the path starts with one of the tokens,
	 * if yes, return 0;
	 * else return -EACCESS;
	 */
	struct file *out = NULL;
	char *buf = NULL;
	char *str = NULL;
	char *token = NULL;
	int ret = 0;
	int size = 4098;
	ret = openfile(&out, CONFIG_TRAN_DEFAULT, O_RDONLY, 0);
	if (ret) {
		goto clean;
	}else{
		if (out->f_dentry->d_inode->i_size > size)
			size = out->f_dentry->d_inode->i_size + 1;
		buf = (char*) kmalloc(sizeof(char)*size, GFP_KERNEL);
		ret = readfile(&out, buf, size);
		if (ret <= 0){
			goto clean;
		}else {
			buf[ret] = '\0';
			str = buf;
			printk("default buf = %s\n", buf);
			while((token = strsep(&str, "\n")) != NULL) {
				if (strcmp(token,"\0") && !strncmp(path, token, strlen(token))) {
					ret = 0;
					goto clean;
				}
			}
			ret = -EACCES;
		}
	}
clean:
	kfree(buf);
	if (!out)
		closefile(&out);
	return ret;
}
int recursive_check(char *buf, char *path, char *op)
{
	int i = 0;
	int ret = 0;
	char *p = NULL;
	char *iter = NULL;
	char *result = NULL;
	char *checkop = NULL;
	if (!buf || !path || !op)
		return -EINVAL;
	p = (char *) kmalloc(strlen(path)+2, GFP_KERNEL);
	if (!p)
		return -ENOMEM;
	strcpy(p, path);
	strcat(p, ":");
check_parent:
	if (p[0] != ':') {
		result = strstr(buf, p);
		if (!result) {
			iter = strrchr(p, '/');
			if (!iter) {
				ret = -EACCES;
				goto clean_check;
			} else{
				iter[0] = ':';
				iter[1] = '\0';
				goto check_parent;
			}
		} else {
			for (i = 0; i < strlen(result); i++) {
				if (result[i] == '\n'){
					result[i] = '\0';
					break;
				}
			}
			checkop = strstr(result, op);
			if (!checkop)
				ret = -EACCES;
			else
				ret = 0;
		}
	} else
		ret = -EACCES;
clean_check:
	kfree(p);
	return ret;
}

bool enforce(char *path)
{
	int ret = 0;
	bool r = false;
	struct file *out = NULL;
	char *buf = NULL;
	char *iter = NULL, *result = NULL;
	int size = 4096;
	if (!path)
		goto on_error;
	ret = openfile(&out, CONFIG_ENFORCE, O_RDONLY, 0);
	if (ret)
		goto on_error;
	else {
		if ( out->f_dentry->d_inode->i_size > size) {
			size = out->f_dentry->d_inode->i_size+1;
		}
		buf = kmalloc(size, GFP_KERNEL);
		if (!buf)
			goto on_error;
		ret = readfile(&out, buf, size);
		if (ret <= 0)
			goto on_error;
		else {
			buf[ret] = '\0';
			iter = buf;
			printk("enforce buf = %s\n", buf);
			while((result = strchr(iter, '\n')) != NULL){
				result[0] = '\0';
				printk("path = %s and iter = %s", path, iter);
				if (!strncmp(path, iter, strlen(iter))){
					r = true;
					break;
				} else {
					iter = &result[1];
				} 
			}
			printk("path = %s and iter = %s", path, iter);
			if (!strncmp(path, iter, strlen(iter))){
                                r = true;
                       	}
		}
	}
on_error:
	kfree(buf);
	if(out)
		closefile(&out);
	if (r)
		printk("true\n");
	else
		printk("false\n");
	return r; 
}
int authorize(char *role, uid_t id, char *op, struct dentry *dentry)
{
	char *path = NULL;
	char *trans = NULL;
	char *buf = NULL;
	int ret = 0;
	int size = 4098;
	struct file *out = NULL;
	if (!strcmp(role, "root")) {
		return 0;
	}
	getpath(dentry, &path);
	if (!enforce(path)) {
		kfree(path);
		return 0;
	}
	if (strcmp(role, "default") && role_exists(role)){
		/*
		 * Open transaction file. If not found goto defstate;
		 * else, check for "path" in file, if not found,
		 * goto fail state, else, check for OP, if found, return 0;
		 * else goto defstate;
		 */
		trans = (char *) kmalloc(strlen(CONFIG_TRAN)+strlen(role)+1, GFP_KERNEL);
		strcpy(trans, CONFIG_TRAN);
		strcat(trans, role);
		ret = openfile(&out, trans, O_RDONLY, 0);
		if (ret) {
			goto defstate;
		}else{
			if(out->f_dentry->d_inode->i_size > size)
				size = out->f_dentry->d_inode->i_size + 1;
			buf = (char*) kmalloc(sizeof(char)*size, GFP_KERNEL);
			ret = readfile(&out, buf, size);
			if (ret <= 0) {
				goto defstate;
			}else {
				buf[ret] = '\0';
				printk("tran buf = %s\n", buf);
				ret = recursive_check(buf, path, op);
				if (ret != 0)
					goto defstate;
				else
					goto cleanup;
			}
		}
	}
defstate:
	ret = check_path_with_default(path);
cleanup:
	kfree(buf);
	if (out)
		closefile(&out);
	kfree(trans);
	kfree(path);
	return ret;	
}

int sbrbac_inode_unlink (struct inode *dir, struct dentry *dentry)
{
	int ret = 0;
	char *role = NULL;
	uid_t id = get_current_user()->uid.val;
	role = (char *)kmalloc(sizeof(char)*256, GFP_KERNEL);
	get_active_role(id, &role);
	printk("active role of user with id =%d is = %s\n", id, role);
	ret = authorize(role, id, "unlink", dentry);
	printk("Authorization value for unlink= %d\n", ret);
	kfree(role);
	return ret;
}

int sbrbac_inode_create(struct inode *dir, struct dentry *dentry, umode_t mode)
{
	int ret = 0;
	char *role = NULL;
	uid_t id = get_current_user()->uid.val;
	role = (char *)kmalloc(sizeof(char)*256, GFP_KERNEL);
	get_active_role(id, &role);
	printk("active role of user with id =%d is = %s\n", id, role);
	ret = authorize(role, id, "create", dentry->d_parent);
	printk("Authorization value for create = %d\n", ret);
	kfree(role);
	return ret;
}
int sbrbac_inode_mkdir(struct inode *dir, struct dentry *dentry, umode_t mode)
{
	int ret = 0;
	char *role = NULL;
	uid_t id = get_current_user()->uid.val;
	role = (char *)kmalloc(sizeof(char)*256, GFP_KERNEL);
	get_active_role(id, &role);
	printk("active role of user with id =%d is = %s\n", id, role);
	ret = authorize(role, id, "mkdir", dentry->d_parent);
	printk("Authorization value for mkdir= %d\n", ret);
	kfree(role);
	return ret;
}
int sbrbac_inode_rmdir(struct inode *dir, struct dentry *dentry)
{
	int ret = 0;
	char *role = NULL;
	uid_t id = get_current_user()->uid.val;
	role = (char *)kmalloc(sizeof(char)*256, GFP_KERNEL);
	get_active_role(id, &role);
	printk("active role of user with id =%d is = %s\n", id, role);
	ret = authorize(role, id, "rmdir", dentry);
	printk("Authorization value for rmdir= %d\n", ret);
	kfree(role);
	return ret;
}
int sbrbac_inode_rename(struct inode *old_dir, struct dentry *old_dentry,
						struct inode *new_dir, struct dentry *new_dentry)
{
	int ret = 0;
	char *role = NULL;
	uid_t id = get_current_user()->uid.val;
	role = (char *)kmalloc(sizeof(char)*256, GFP_KERNEL);
	get_active_role(id, &role);
	printk("active role of user with id =%d is = %s\n", id, role);
	ret = authorize(role, id, "rename", old_dentry);
	printk("Authorization value for rename = %d\n", ret);
	kfree(role);
	return ret;
}
int sbrbac_inode_link(struct dentry *old_dentry,
			   struct inode *dir, struct dentry *new_dentry)
{
	int ret = 0;
	char *role = NULL;
	uid_t id = get_current_user()->uid.val;
	role = (char *)kmalloc(sizeof(char)*256, GFP_KERNEL);
	get_active_role(id, &role);
	printk("active role of user with id =%d is = %s\n", id, role);
	ret = authorize(role, id, "hardlink", old_dentry);
	printk("Authorization value for link = %d\n", ret);
	kfree(role);
	return ret;
}
int sbrbac_inode_symlink(struct inode *dir,
			      struct dentry *dentry, const char *old_name)
{
	int ret = 0;
	char *role = NULL;
	uid_t id = get_current_user()->uid.val;
	role = (char *)kmalloc(sizeof(char)*256, GFP_KERNEL);
	get_active_role(id, &role);
	printk("active role of user with id =%d is = %s\n", id, role);
	ret = authorize(role, id, "symlink", dentry);
	printk("Authorization value for symlink= %d\n", ret);
	kfree(role);
	return ret;
}
				   
static struct security_operations sbrbac_ops = {
	.name =			"sbrbac",
	.inode_unlink 	=	sbrbac_inode_unlink,
	.inode_create	= 	sbrbac_inode_create,
	.inode_mkdir	=	sbrbac_inode_mkdir,
	.inode_rmdir	=	sbrbac_inode_rmdir,
	.inode_rename	=	sbrbac_inode_rename,
	.inode_link		=	sbrbac_inode_link,
	.inode_symlink	=	sbrbac_inode_symlink,
};

static __init int sbrbac_init(void)
{
	if (register_security(&sbrbac_ops)) {
		printk(KERN_ERR "SBRBAC: Failed to load\n");
		return -EINVAL;
	}
	printk(KERN_INFO "SBRBAC Loaded\n");
	return 0;
}

static  __exit void sbrbac_exit(void)
{
	printk(KERN_INFO "SBRBAC: Successfully unloaded");
	return;
}

module_init(sbrbac_init);
module_exit(sbrbac_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR(AUTHOR);
